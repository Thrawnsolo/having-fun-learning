/**
 * Created by Alfonso on 3/11/2017.
 */
var config = config || {};

(function bola() {
    //var obj;
    var elementBola = document.getElementById('principal');

    var onPrincipalClick = function () {
        alert('Hello, world!');
    };
/*
    elementBola.addEventListener('click', onPrincipalClick);
    document.onkeydown = function (event) {
        var keyPressed  = event.key;
        var elementWidth = elementBola.offsetWidth;
        var elementHeight = elementBola.offsetHeight;
        if(keyPressed === 'ArrowUp'){
            elementBola.style.setProperty('width', (elementWidth+1)+'px');
        }
        if(keyPressed === 'ArrowDown'){
            elementBola.style.setProperty('width', (elementWidth-1)+'px');
        }
        if(keyPressed === 'ArrowLeft'){
            elementBola.style.setProperty('height', (elementHeight-1)+'px');
        }
        if(keyPressed === 'ArrowRight'){
            elementBola.style.setProperty('height', (elementHeight+1)+'px');
        }
    };
*/
    document.onkeydown = function (event) {
        var keyPressed  = event.key;
        var elementTop = elementBola.offsetTop;
        var elementLeft = elementBola.offsetLeft;
        if(keyPressed === 'ArrowUp'){
            elementBola.style.setProperty('top', (elementTop-5)+'px');
        }
        if(keyPressed === 'ArrowDown'){
            elementBola.style.setProperty('top', (elementTop+5)+'px');
        }
        if(keyPressed === 'ArrowLeft'){
            elementBola.style.setProperty('left', (elementLeft-5)+'px');
        }
        if(keyPressed === 'ArrowRight'){
            elementBola.style.setProperty('left', (elementLeft+5)+'px');
        }
    };

    document.onclick = function (event) {
        var newDiv = document.createElement('DIV');
        document.body.appendChild(newDiv);
        newDiv.className = 'circle';
        newDiv.style.backgroundColor = 'blue';
        newDiv.style.left = event.clientX.toString() +'px';
        newDiv.style.top = event.clientY.toString() +'px';
    };

    //return obj;
})()