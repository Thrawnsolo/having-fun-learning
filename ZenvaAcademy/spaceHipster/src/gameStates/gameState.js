/**
 * Created by Alfonso on 7/29/2017.
 */
define(function (require) {
    var PlayerBullet = require ('playerBullet');

    var Enemy = require ('enemy');
    return function GameState (p) {
        var obj = {};
        var phaser = p;
        var bulletCount = 0;
        var currentEnemyIndex = 0;
        obj.init = function (currentLevel) {
            //De este modo lo escalamos y se muestra todo
            this.scale.scaleMode = phaser.ScaleManager.SHOW_ALL;

            this.game.physics.startSystem(phaser.Physics.ARCADE);

            this.PLAYER_SPEED = 200;
            this.BULLET_SPEED = -1000;

            //level data
            this.numLevels = 3;
            this.currentLevel = currentLevel ? currentLevel : 1;
        };

        obj.preload = function () {
            this.load.image('space', '../assets/images/space.png');
            this.load.image('player', '../assets/images/player.png');
            this.load.image('bullet', '../assets/images/bullet.png');
            this.load.image('enemyParticle', '../assets/images/enemyParticle.png');

            this.load.spritesheet('yellowEnemy', '../assets/images/yellow_enemy.png', 50, 46, 3, 1, 1);
            this.load.spritesheet('redEnemy', '../assets/images/red_enemy.png', 50, 46, 3, 1, 1);
            this.load.spritesheet('greenEnemy', '../assets/images/green_enemy.png', 50, 46, 3, 1, 1);

            //load level data
            this.load.text('level1', '../assets/data/level1.json');
            this.load.text('level2', '../assets/data/level2.json');
            this.load.text('level3', '../assets/data/level3.json');

            this.load.audio('orchestra', ['../assets/audio/8bit-orchestra.mp3', '../assets/audio/8bit-orchestra.ogg']);

        };

        obj.create = function () {
            //Creamos un tilesprite para que el background sean las estrellas repetidas
            this.background = this.add.tileSprite(0, 0, this.game.world.width, this.game.world.height, 'space');
            //Y para moverlo usamos el metodo autoscroll, que en este caso lo queremos mover en Y, asi que le asignamos velocidad de 30
            this.background.autoScroll(0, 30);

            this.player = this.add.sprite(this.game.world.centerX, this.world.height - 50, 'player');
            this.player.anchor.setTo(0.5);
            this.game.physics.arcade.enable(this.player);
            this.player.body.collideWorldBounds = true;

            //Creamos la pool de balas
            obj.initBullets();
            //Shooting timer
            this.game.time.events.loop(phaser.Timer.SECOND/5, createPlayerBullet, this);
            //Pool de enemigos
            obj.initEnemies();
            //Cargamos nivel

            obj.loadLevel();

            this.orchestra = this.add.audio('orchestra');
            this.orchestra.play();
        };

        function damageEnemy(bullet, enemy) {
            enemy.damage(1);
            bullet.kill();
        }

        function killPlayer() {
            this.player.kill();
            this.game.state.start('GameState', true, false, this.currentLevel);
            this.orchestra.stop();
        }

        obj.loadLevel = function () {
            currentEnemyIndex = 0;

            this.levelData = JSON.parse(this.game.cache.getText('level' + this.currentLevel));

            //end of the level timer
            this.game.time.events.add(this.levelData.duration * 1000, function () {
                this.orchestra.stop();
                //level ends
                if(this.currentLevel < this.numLevels) {
                    this.currentLevel++;
                } else {
                    this.currentLevel = 1;
                }
                this.game.state.start('GameState', true, false, this.currentLevel);

            }, this);

            obj.scheduleNextEnemy();
        };

        obj.scheduleNextEnemy  = function() {
            var nextEnemy = this.levelData.enemies[currentEnemyIndex];
            if(nextEnemy) {
                var nextTime = 1000 * (nextEnemy.time - (currentEnemyIndex === 0 ? 0 :  this.levelData.enemies[currentEnemyIndex-1].time));
                //Enemy timer
                this.game.time.events.add(nextTime, function () {
                    obj.createEnemy(nextEnemy.x * this.game.world.width, -100, nextEnemy.health, nextEnemy.key, nextEnemy.scale, nextEnemy.speedX, nextEnemy.speedY );
                    currentEnemyIndex++;
                    obj.scheduleNextEnemy();
                }, this);
            }
        };

        obj.update = function () {
            //Inicializamos el overlap de las bullets yu ;los enemigos
            this.game.physics.arcade.overlap(this.playerBullets, this.enemies, damageEnemy, null, this);

            this.game.physics.arcade.overlap(this.enemyBullets, this.player, killPlayer, null, this);

            this.player.body.velocity.x = 0;
            this.player.body.velocity.y = 0;
            //Decimos que este atento a que se toque la pantalla
            if(this.game.input.activePointer.isDown) {
                //Aqui lo movemos a la direccion en x que este el active pointer
                var targetX = this.game.input.activePointer.position.x;
                var targetY = this.game.input.activePointer.position.y;
                if (this.player.body.position.y !== targetY && this.player.body.position.x !== targetX) {
                    var directionX = targetX >= this.player.body.position.x ? 1 : -1;
                    var directionY = targetY >= this.player.body.position.y ? 1 : -1;
                    this.player.body.velocity.x = directionX * this.PLAYER_SPEED;
                    this.player.body.velocity.y = directionY * this.PLAYER_SPEED;
                }
            }
        };

        obj.initBullets = function() {
            this.playerBullets = this.add.group();
            this.playerBullets.enableBody = true;
        };

        obj.initEnemies = function () {
            this.enemies = this.add.group();
            this.enemies.enableBody = true;

            this.enemyBullets = this.add.group();
            this.enemyBullets.enableBody = true;
        };

        function createPlayerBullet() {
            var bullet = this.playerBullets.getFirstExists(false);
            if(!bullet){
                bulletCount++;
                bullet = new PlayerBullet(this.game, this.player.x, this.player.top);
                //la aniadimos al pool
                this.playerBullets.add(bullet);
            } else {
                //hacemos reset a la posicion
                bullet.reset(this.player.x, this.player.top);
            }
            bullet.body.velocity.y = this.BULLET_SPEED;
        }

        obj.createEnemy = function (x, y, health, key, scale, speedX, speedY) {
            var enemy = this.enemies.getFirstExists(false);

            if (!enemy) {
                enemy = new Enemy(this.game, x, y, key, health, this.enemyBullets);
                this.enemies.add(enemy);
            }
            enemy.reset(x, y, health, key, scale, speedX, speedY);
        };

        return obj;
    }
});
