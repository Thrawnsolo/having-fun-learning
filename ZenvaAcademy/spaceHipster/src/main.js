/**
 * Created by Alfonso on 7/16/2017.
 */
define(function(require) {
    var spaceHipster = {};
    'use strict';
    var phaser = require('phaser');
    //var BootState = require('bootState');
    //var PreloadState = require('preloadState');
    var GameState = require('gameState');

    var game = new phaser.Game('100%', '100%', phaser.AUTO);

    //game.state.add('BootState', BootState(phaser));
    //game.state.add('PreloadState', PreloadState());
    game.state.add('GameState', GameState(phaser));
    //game.state.start('BootState');
    game.state.start('GameState');
});
