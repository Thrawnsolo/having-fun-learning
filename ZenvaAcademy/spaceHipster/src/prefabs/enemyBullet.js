/**
 * Created by Alfonso on 7/30/2017.
 */
define(function (require){
    var phaser = require('phaser');
    return function EnemyBullet (game, x, y) {
        var obj = new phaser.Sprite(game, x, y, 'bullet');
        obj.anchor.setTo(0.5);
        obj.checkWorldBounds = true;
        obj.outOfBoundsKill = true;

        return obj;
    }
});
