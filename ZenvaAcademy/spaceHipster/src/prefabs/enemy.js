/**
 * Created by Alfonso on 7/29/2017.
 */
define(function (require){
    var phaser = require('phaser');
    var EnemyBullet = require ('enemyBullet');
    return function Enemy (game, x, y, key, health, enemyBullets) {
        var obj = new phaser.Sprite(game, x, y, key);
        obj.animations.add('getHit', [0, 1, 2, 1, 0], 25, false);

        //Le aniadimos fisicas al enemigo
        //recuerda que hay un bug que si se aniade a un grupo y se enable las fisicas en grupo, entonces deja de funcionar, por eso lo comentamos
        //game.physics.arcade.enable(obj);

        obj.anchor.setTo(0.5);
        //Esto lo trae phaser de serie :D
        obj.health = health;
        obj.enemyBullets = enemyBullets;
        //Metemos un timer para que dispare cada x. False significa que no va a destruir el timer tras lanzarse

        obj.enemyTimer = game.time.create(false);
        obj.enemyTimer.start();

        scheduleShooting();

        /*obj.checkWorldBounds = true;
        obj.outOfBoundsKill = true;*/

        obj.update = function () {
            if(obj.x < Math.floor(0.053 * game.world.width)){
                obj.x = Math.floor(0.053 * game.world.width + 15);
                obj.body.velocity.x *= -1;
            } else if (obj.x > Math.floor(0.95 * game.world.width)) {
                obj.x = Math.floor(0.95 * game.world.width - 15);
                obj.body.velocity.x *= -1;
            }
            if(obj.position.y > game.world.height){
                obj.kill();
            }
        };

        obj.damage = function (amount) {
            //debugger
            //Llamamos a la funciton padre para que haga lo suyo (better call your parent)
            phaser.Sprite.prototype.damage.call(obj, amount);
            obj.play('getHit');

            //particle explosion
            if(obj.health <= 0) {
                var emitter = game.add.emitter(obj.x, obj.y, 100);
                emitter.makeParticles('enemyParticle');
                emitter.minParticleSpeed.setTo(-200, -200);
                emitter.maxParticleSpeed.setTo(200, 200);
                emitter.gravity = 0;
                //primer parametro, queremos explosion? si, true, cuanto tiempo saldran las particulas, frecuencia
                emitter.start(true, 500, null, 100);
                //tenemos que apagar el timer
                obj.enemyTimer.pause();
            }
        };

        obj.reset = function (x, y, health, key, scale, speedX, speedY) {
            //Llamamos a la funciton padre para que haga lo suyo (better call your parent)
            phaser.Sprite.prototype.reset.call(obj, x, y, health);
            obj.loadTexture(key);
            obj.scale.setTo(scale);

            obj.body.velocity.x = speedX;
            obj.body.velocity.y = speedY;
            //Y lo arrancamos cuando renace
            obj.enemyTimer.resume();
        };

        function scheduleShooting () {
            shoot();
            obj.enemyTimer.add(phaser.Timer.SECOND * 2, scheduleShooting, this);
        }
        function shoot () {
            var bullet = obj.enemyBullets.getFirstExists(false);
            if(!bullet) {
                bullet = new EnemyBullet(game, obj.x, obj.bottom);
                obj.enemyBullets.add(bullet);
            } else {
                bullet.reset(obj.x, obj.y);
            }
            bullet.body.velocity.y = 100;
        }

        return obj;
    }
});