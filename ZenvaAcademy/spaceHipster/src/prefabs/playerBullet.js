define(function (require){
    var phaser = require('phaser');
    return function PlayerBullet (game, x, y) {
        var obj = new phaser.Sprite(game, x, y, 'bullet');
        obj.anchor.setTo(0.5);
        obj.checkWorldBounds = true;
        obj.outOfBoundsKill = true;

        return obj;
    }
});