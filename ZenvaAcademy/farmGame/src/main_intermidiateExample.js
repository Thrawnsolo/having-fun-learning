/**
 * Created by Alfonso on 6/3/2017.
 */
var game = new Phaser.Game(640, 360, Phaser.AUTO);

var GameState = {
    //There aer many states that we are not using here, such as:
    // init: the very first function to be called, even before preloading.
    //can be used to load variables or whatever.
    preload: function () {
        //preload assets
        this.load.image('background', 'assets/images/background.png');
        this.load.image('horse', 'assets/images/horse.png');
        this.load.image('chicken', 'assets/images/chicken.png');
        this.load.image('pig', 'assets/images/pig.png');
        this.load.image('sheep', 'assets/images/sheep3.png');
        this.load.image('arrow', 'assets/images/arrow.png');
    },
    create: function () {

        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = true;


        //group of animals:

        var animalData = [
            {key: 'chicken', text: 'CHICKEN'},
            {key: 'horse', text: 'HORSE'},
            {key: 'pig', text: 'PIG'},
            {key: 'sheep', text: 'SHEEP'}
        ];


        //create the actual game, called once preload is completed
        this.background = this.game.add.sprite(0,0, 'background');
        this.pig = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'pig');
        this.pig.anchor.setTo(0.5);


        this.pig.inputEnabled = true;
        this.pig.input.pixelPerfectClick = true;
        this.pig.events.onInputDown.add(this.animateAnimal, this);


        //Left arrow
        this.leftArrow = this.game.add.sprite(60, this.game.world.centerY, 'arrow');
        this.leftArrow.anchor.setTo(0.5);
        //this.leftArrow.scale.setTo(-1, 1);
        //este de abajo es equivalente al de arriba
        this.leftArrow.scale.x = -1;
        this.leftArrow.customParams = {direction: 1};


        //left arrow allow user iunut
        this.leftArrow.inputEnabled = true;
        this.leftArrow.input.pixelPerfectClick = true;
        this.leftArrow.events.onInputDown.add(this.switchAnimal, this);

        //Right arrow
        this.rightArrow = this.game.add.sprite(580, this.game.world.centerY, 'arrow');
        this.rightArrow.anchor.setTo(0.5);
        this.rightArrow.customParams = {direction: 1};

        this.rightArrow.inputEnabled = true;
        this.rightArrow.input.pixelPerfectClick = true;
        this.rightArrow.events.onInputDown.add(this.switchAnimal, this);
    },
    update: function () {
        //constantly update the game in order to refresh images and stage
        //has been left empty for our own use. It is called during the core game loop after debug, physics, plugins and stage had their preupdate method called.
    },

    switchAnimal: function (sprite, event) {
        console.log('moving!');
    },
    animateAnimal: function (sprite, event) {
        console.log('animating animal!');
    }

};

game.state.add('GameState', GameState);
game.state.start('GameState');
