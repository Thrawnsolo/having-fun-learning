/**
 * Created by Alfonso on 6/3/2017.
 */
    var game = new Phaser.Game(640, 360, Phaser.AUTO);

    var GameState = {
        //There aer many states that we are not using here, such as:
        // init: the very first function to be called, even before preloading.
        //can be used to load variables or whatever.
        preload: function () {
            //preload assets
            this.load.image('background', 'assets/images/background.png');
            this.load.image('arrow', 'assets/images/arrow.png');


            //Solamente necesitamos introducir el widht, heigth y numero de sprites dentro del spritesheet.
            this.load.spritesheet('horse', 'assets/images/horse_spritesheet.png', 212, 200, 3);
            this.load.spritesheet('chicken', 'assets/images/chicken_spritesheet.png', 131, 200, 3);
            this.load.spritesheet('pig', 'assets/images/pig_spritesheet.png', 297, 200, 3);
            this.load.spritesheet('sheep', 'assets/images/sheep_spritesheet.png', 244, 200, 3);

            this.load.audio('chickenSound', ['assets/audio/chicken.mp3', 'assets/audio/chicken.ogg']);
            this.load.audio('horseSound', ['assets/audio/horse.mp3', 'assets/audio/horse.ogg']);
            this.load.audio('pigSound', ['assets/audio/pig.mp3', 'assets/audio/pig.ogg']);
            this.load.audio('sheepSound', ['assets/audio/sheep.mp3', 'assets/audio/sheep.ogg']);
        },
        create: function () {
            //create the actual game, called once preload is completed
            var self = this;
            var animal;

            //Scaling options
            self.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            //Have the game centered horizontally
            self.scale.pageAlignHorizontally = true;
            self.scale.pageAlignVertically = true;

            //sprite for the background
            self.background = self.game.add.sprite(0,0, 'background');

            //group of animals:
            var animalData = [
                {key: 'chicken', text: 'CHICKEN', audio: 'chickenSound'},
                {key: 'horse', text: 'HORSE', audio: 'horseSound'},
                {key: 'pig', text: 'PIG', audio: 'pigSound'},
                {key: 'sheep', text: 'SHEEP', audio: 'sheepSound'}
            ];

            //We add a new group

            self.animals = self.game.add.group();

            //We store the previously created object into the new group by using create.

            animalData.forEach(function(element){
                //Ahora, como usamos el spritesheet, debemos indicar en el momento de la creacion un parametro adicional: el frame inicial
                animal = self.animals.create(-1000, self.game.world.centerY, element.key, 0);
                animal.anchor.setTo(0.5);

                //Aqui podemos meter parametros adicionales, como el text asignado a la imagen o su audio
                animal.customParams = {text: element.text, sound: self.game.add.audio(element.audio)};

                //Aqui creamos la animacion para ese sprite
                //El primer parametro es el nombre de la animacion. Aqui le ponemos animate pero puede ser cualquiera
                //Luego viene el orden dentro del spritesheet en el que los sprites seran mostrados, luego el numero de milisegundos quedurara
                //y un boolean con el looping
                animal.animations.add('animate', [0,1,2,1,0,1,0], 3, false);


                animal.inputEnabled = true;
                animal.input.pixelPerfectClick = true;
                animal.events.onInputDown.add(self.animateAnimal, self);
            });

            //En el momento en el que usamos un grupo, tenemos a nuestra disposicion los metodos next  y previous, que devuelven el elemento
            //siguiente y anterior.

            self.currentAnimal = self.animals.next();

            //Ahora mostramos el animal actual
            self.currentAnimal.position.set(self.game.world.centerX, self.game.world.centerY);

            //Y su texto

            this.showText(this.currentAnimal);

            //Left arrow
            self.leftArrow = self.game.add.sprite(60, self.game.world.centerY, 'arrow');
            self.leftArrow.anchor.setTo(0.5);
            //this.leftArrow.scale.setTo(-1, 1);
            //este de abajo es equivalente al de arriba
            self.leftArrow.scale.x = -1;
            self.leftArrow.customParams = {direction: -1};

            //left arrow allow user input
            self.leftArrow.inputEnabled = true;
            self.leftArrow.input.pixelPerfectClick = true;
            self.leftArrow.events.onInputDown.add(self.switchAnimal, self);

            //Right arrow
            self.rightArrow = this.game.add.sprite(580, self.game.world.centerY, 'arrow');
            self.rightArrow.anchor.setTo(0.5);
            self.rightArrow.customParams = {direction: 1};

            self.rightArrow.inputEnabled = true;
            self.rightArrow.input.pixelPerfectClick = true;
            self.rightArrow.events.onInputDown.add(self.switchAnimal, self);
        },
        update: function () {
            //constantly update the game in order to refresh images and stage
            //has been left empty for our own use. It is called during the core game loop after debug, physics, plugins and stage had their preupdate method called.
        },

        switchAnimal: function (sprite, event) {
            //Get direction of arrow, get next animal, final destination of current, move it, set next animal as current.

            if(this.isMoving){
                return false;
            }

            this.isMoving = true;

            this.animalText.visible = false;


            var direction = sprite.customParams.direction;
            var animal;
            var endX;
            var newAnimalMovement;
            var currentAnimalMovement;

            if(direction > 0){
                animal = this.animals.next();
                animal.x = -animal.width/2;
                endX = 640 + this.currentAnimal.width/2;
            }else{
                animal = this.animals.previous();
                animal.x = 640 + animal.width/2;
                endX = -this.currentAnimal.width/2;
            }

            newAnimalMovement = this.game.add.tween(animal);
            newAnimalMovement.to({x: this.game.world.centerX}, 1000);

            newAnimalMovement.onComplete.add(function () {
                this.isMoving = false;
                this.showText(animal);
            }, this);

            newAnimalMovement.start();

            currentAnimalMovement = this.game.add.tween(this.currentAnimal);
            currentAnimalMovement.to({x: endX}, 1000);
            currentAnimalMovement.start();

            this.currentAnimal = animal;
        },
        animateAnimal: function (sprite, event) {
            //El sprite puede tener varias animaciones.
            //Cuando le decimos play, le tenemos que decir cual de ellas queremos que se reproduzca.
            sprite.play('animate');
            sprite.customParams.sound.play();
        },

        showText: function (animal) {
            if(!this.animalText){
                var style = {
                    font: 'bold 30pt Arial',
                    fill: '#FF0000',
                    align: 'center'
                };
                this.animalText = this.game.add.text(this.game.width/2, this.game.height * 0.85, '', style);
                this.animalText.anchor.setTo(0.5);
            }

            this.animalText.setText(animal.customParams.text);
            this.animalText.visible = true;
        }
    };

    game.state.add('GameState', GameState);
    game.state.start('GameState');

