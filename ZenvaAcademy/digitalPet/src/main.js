/**
 * Created by Alfonso on 6/4/2017.
 */
//initiate the phaser framework
var game = new Phaser.Game(360, 640, Phaser.AUTO);

game.state.add('GameState', GameState);
game.state.add('BootState', BootState);
game.state.add('PreloadState', PreloadState);
game.state.add('HomeState', HomeState);

game.state.start('BootState');
