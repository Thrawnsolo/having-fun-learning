/**
 * Created by Alfonso on 6/17/2017.
 */
var HomeState = {

    init: function (message){
        this.message = message;
    },
    create: function () {
     var background = this.game.add.sprite(0,0,'backyard');
     background.inputEnabled = true;

     var style = {
         font: '35px Arial',
         fill: '#fff'
     };

     this.game.add.text(30, this.game.world.centerY + 200, 'Touch to Start', style);

     if(this.message){
         this.game.add.text(60, this.game.world.centerY - 200, this.message, style);
     }

     background.events.onInputDown.add(function (){
        this.state.start('GameState');
     }, this);
    }
};