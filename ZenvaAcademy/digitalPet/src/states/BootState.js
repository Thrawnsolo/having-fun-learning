/**
 * Created by Alfonso on 6/17/2017.
 */
var BootState = {
    //initiate some game-level settings
    init: function () {
        //Mantiene la escala del juego cuando hagamsos resize
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = true;
    },
    //load the game assets before the game starts
    preload: function () {
        this.load.image('logo', 'assets/images/logo.png');
        this.load.image('bar', 'assets/images/bar.png');
    },
    create: function () {
        this.game.stage.backgroundColor = '#fff';

        this.state.start('PreloadState');
    }
};

