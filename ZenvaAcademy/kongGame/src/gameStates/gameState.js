/**
 * Created by Alfonso on 6/17/2017.
 */
define(function () {
    return function GameState(p) {
        var obj = {};
        var platform;
        var cursors;
        var game;
        var ground;
        var player;
        var goal;
        var leftArrow;
        var rightArrow;
        var actionButton;
        var platforms;
        var levelData;
        var fires;
        var RUNNING_SPEED = 180;
        var JUMPING_SPEED = 550;
        var spaceBar;
        var fire;
        var barrelCreator;
        var barrels;
        var enablePlayerCollide;
        var playerDiesAnimation;
        var playerWinsAnimation;
        var phaser = p;
        //Usamos un pool of objects para reutilizar elementos que se han usado previamente para que sea mas eficiente.
        //En este caso lo haremos con los barriles, habra un numero limitado.

        obj.create = function () {
            game = this.game;
            ground = this.add.sprite(0, 638, 'ground');

            cursors = game.input.keyboard.createCursorKeys();
            spaceBar = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

            game.physics.arcade.enable(ground);

            //Aqui parseamos el fichero de niveles
            levelData = JSON.parse(game.cache.getText('level'));

            //Starting place of the player
            player = this.add.sprite(levelData.playerStart.x, levelData.playerStart.y, 'player', 3);
            //Una vez declarado el jugador, queremos que la pantalla lo siga:
            game.camera.follow(player);

            player.customParams = {};
            player.customParams.isDead = false;

            enablePlayerCollide = true;

            playerDiesAnimation = game.add.tween(player);

            goal = this.add.sprite(levelData.goal.x, levelData.goal.y, 'goal');

            //enablebody es para grupos, enable es para un single sprite
            game.physics.arcade.enable(goal);
            goal.body.allowGravity = false;

            playerWinsAnimation  = game.add.tween(goal);
            //Fires
            fires = this.add.group();
            fires.enableBody = true;

            levelData.fireData.forEach(function (element) {
                fire = fires.create(element.x, element.y, 'fire');
                fire.animations.add('fire', [0, 1], 4, true);
                fire.play('fire');
            });

            fires.setAll('body.allowGravity', false);

            barrels = this.add.group();
            barrels.enableBody = true;

            barrelCreator = game.time.events.loop(Phaser.Timer.SECOND * levelData.barrelFrequency, createBarrel, this);

            //Introducimos el concepto grupos, mediante el cual se pueden aniadir eventos y propiedades globales a muchos objetos del mismo tipo
            platforms = this.add.group();
            platforms.enableBody = true;

            levelData.platformData.forEach(function (element) {
                platforms.create(element.x, element.y, 'platform');
            }, this);

            //Con esto conseguimos lo mismo que lo que se consigue individualmente con el codigo de debajo
            platforms.setAll('body.immovable', true);
            platforms.setAll('body.allowGravity', false);

            //Esto hace que la gravedad no se aplique al objeto.
            ground.body.allowGravity = false;
            //Esto hace un objeto inmovil
            ground.body.immovable = true;

            //Asi asignamos la fisica y la gravedad a un elemento en particular
            game.physics.arcade.enable(player);
            player.anchor.setTo(0.5);
            player.animations.add('walking', [0, 1, 2, 1], 6, true);

            createOnscreenControls.apply(this);
        };
        obj.update = function () {
            //Con este metodo asignamos que pbjetos colisionan entre si. Se le puiede asignar una funcion callback, landed en nuestro caso

            game.physics.arcade.collide(player, ground, landed, processCollisionCallback);
            //Si quieres que dos elementos puedan hacer overlap
            //game.physics.arcade.overlap(player, platform, this.landed);
            //Estas son las dos maneras de manejar las colisiones en phaser.
            game.physics.arcade.collide(player, platforms, landed, processCollisionCallback);

            //Vamos a meterle overlap al jugador con los fuegos, para que se detecte contacto y muerte
            game.physics.arcade.overlap(player, fires, killPlayer, processCollisionCallback);

            game.physics.arcade.overlap(player, goal, winGame, processCollisionCallback);

            game.physics.arcade.collide(barrels, platforms, landed);
            game.physics.arcade.collide(barrels, ground, landed);

            game.physics.arcade.collide(player, barrels, killPlayer, processCollisionCallback);

            //Asignamos una velocidad inicial de cero
            player.body.velocity.x = 0;
            player.body.collideWorldBounds = true;

            if(!player.customParams.isDead) {
                if (cursors.left.isDown || player.customParams.isMovingLeft) {
                    //Lo ponemos del derecho
                    player.scale.setTo(1, 1);
                    player.play('walking');
                    player.body.velocity.x = -RUNNING_SPEED;
                } else if (cursors.right.isDown || player.customParams.isMovingRight) {
                    //Le damos la vuelta al sprite
                    player.scale.setTo(-1, 1);
                    player.play('walking');
                    player.body.velocity.x = RUNNING_SPEED;
                } else {
                    player.animations.stop();
                    //Le ponemos el frame de quieto
                    player.frame = 3;
                }

                //Tenemos que comprobar que solo puede saltar cuando este encima de un objeto, de ahi la segunda condicion
                if ((cursors.up.isDown || spaceBar.isDown || player.customParams.mustJump) && player.body.touching.down) {
                    player.body.velocity.y = -JUMPING_SPEED;
                }
            }
            //eliminamos los barriles cuando lleguen a cierta area
            barrels.forEach(function (element) {
                if(element.x < 1 && element.y > 600) {
                    element.kill();
                }
            });
        };

        function processCollisionCallback() {
            return enablePlayerCollide;
        }
        function createBarrel () {
            //Phaser mantiene un track de los dead sprites y puedes recuperar aquellos que estan muertos para reusarlos
            var barrel = barrels.getFirstExists(false);
            if(!barrel){
                barrel = barrels.create(0, 0, 'barrel');
            }
            barrel.reset(levelData.goal.x, levelData.goal.y);

            barrel.body.velocity.x = levelData.barrelSpeed;

            barrel.body.collideWorldBounds = true;
            //Cuando choque, que rebote
            barrel.body.bounce.set(1,0);

        }
        function createOnscreenControls () {
            leftArrow = this.add.button(20, 590, 'arrowButton');
            rightArrow = this.add.button(110, 590, 'arrowButton');
            actionButton = this.add.button(280, 590, 'actionButton');

            leftArrow.alpha = 0.5;
            rightArrow.alpha = 0.5;
            actionButton.alpha = 0.5;

            //With this, the controls will follow the player, they will be fixed
            leftArrow.fixedToCamera = true;
            rightArrow.fixedToCamera = true;
            actionButton.fixedToCamera = true;

            actionButton.events.onInputDown.add(function () {
                player.customParams.mustJump = true;
            }, this);

            actionButton.events.onInputUp.add(function () {
                player.customParams.mustJump = false;
            }, this);


            leftArrow.events.onInputDown.add(function () {
                player.customParams.isMovingLeft = true;
            }, this);

            leftArrow.events.onInputUp.add(function () {
                player.customParams.isMovingLeft = false;
            }, this);


            //Estos extra son para los moviles, que funcione bien bajo presion continua
            leftArrow.events.onInputOver.add(function () {
                player.customParams.isMovingLeft = true;
            }, this);

            leftArrow.events.onInputOut.add(function () {
                player.customParams.isMovingLeft = false;
            }, this);


            rightArrow.events.onInputDown.add(function () {
                player.customParams.isMovingRight = true;
            }, this);

            rightArrow.events.onInputUp.add(function () {
                player.customParams.isMovingRight = false;
            }, this);

            //Estos extra son para los moviles, que funcione bien bajo presion continua
            rightArrow.events.onInputOver.add(function () {
                player.customParams.isMovingRight = true;
            }, this);

            rightArrow.events.onInputOut.add(function () {
                player.customParams.isMovingRight = false;
            }, this);
        }
        function landed (player, ground) {
        }
        function killPlayer (player) {
            enablePlayerCollide = false;
            player.body.collideWorldBounds = false;
            player.customParams.isDead = true;
            game.world.bringToTop(player);
            playerDiesAnimation.to({y: player.y - 150}, 1000, phaser.Easing.Exponential.InOut).to({y: player.y + 600}, 500, phaser.Easing.Exponential.InOut);
            playerDiesAnimation.onComplete.add(function () {
                game.state.start('GameState');
            });
            playerDiesAnimation.start();
        }
        function winGame (player, goal) {
            player.customParams.isDead = true;
            game.world.bringToTop(goal);
            playerWinsAnimation.to({y: goal.y + 600}, 1000, phaser.Easing.Exponential.InOut);
            playerWinsAnimation.onComplete.add(function () {
                game.state.start('GameState');
            });
            playerWinsAnimation.start();
        }

        return obj;
    }
});


