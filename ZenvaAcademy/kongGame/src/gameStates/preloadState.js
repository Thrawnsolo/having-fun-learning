/**
 * Created by Alfonso on 6/17/2017.
 */
define(function () {
    return function PreloadState () {
        var obj = {};
        obj.preload = function () {
            this.load.image('ground', 'assets/images/ground.png');
            this.load.image('platform', 'assets/images/platform.png');
            this.load.image('goal', 'assets/images/gorilla3.png');
            this.load.image('arrowButton', 'assets/images/arrowButton.png');
            this.load.image('actionButton', 'assets/images/actionButton.png');
            this.load.image('barrel', 'assets/images/barrel.png');

            this.load.spritesheet('player', 'assets/images/player_spritesheet.png', 28, 30, 5, 1, 1);
            this.load.spritesheet('fire', 'assets/images/fire_spritesheet.png', 20, 21, 2, 1, 1);

            //We load the levels
            this.load.text('level', 'assets/data/level.json');
        };

        obj.create = function () {
            this.state.start('GameState');
        };
        return obj;
    }
});