/**
 * Created by Alfonso on 6/17/2017.
 */
define(function () {
    return function BootState (phaser) {
       var obj = {};
       obj.init = function () {
           //Mantiene la escala del juego cuando hagamsos resize
           this.scale.scaleMode = phaser.ScaleManager.SHOW_ALL;
           this.scale.pageAlignHorizontally = true;
           this.scale.pageAlignVertically = true;

           //para inicializar las fisicas:
           this.game.physics.startSystem(Phaser.Physics.ARCADE);
           //la gravedad tb puede ser global o especifica de elementos, en este caso es global
           //tb puede usarse para x, y el valor a asignar es por prueba y error, por ver cual te gusta mas;
           this.game.physics.arcade.gravity.y = 1000;


           //La variable world de game sera el mundo del juego. Le podemos asignar el tamanio aqui
           //Le vamos a dar el mismo ancho que la pantalla, pero el alto es mayor, asi que querremos que la camara siga al jugador. Eso lo haremos
           //en la declaracion del jugador.
           this.game.world.setBounds(0, 0, 360, 700);

       };
       //load the game assets before the game starts
       obj.preload =  function () {
           this.load.image('logo', 'assets/images/gorilla3.png');
           this.load.image('bar', 'assets/images/bar.png');
       };

       obj.create = function () {
           this.game.stage.backgroundColor = '#000';

           this.state.start('PreloadState');
       };

       return obj;
   }
});



