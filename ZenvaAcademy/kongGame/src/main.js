/**
 * Created by Alfonso on 6/4/2017.
 */
//initiate the phaser framework
define(function(require) {
    'use strict';
    var phaser = require('phaser');
    var BootState = require('bootState');
    var PreloadState = require('preloadState');
    var GameState = require('gameState');
    var game = new phaser.Game(360, 640, phaser.AUTO);

    game.state.add('BootState', BootState(phaser));
    game.state.add('PreloadState', PreloadState());
    game.state.add('GameState', GameState(phaser));
    game.state.start('BootState');
});